# Maintainer: Benjamin Denhartog <ben@sudoforge.com>
# Contributor: Marc Plano-Lesay <marc.planolesay@gmail.com>

pkgname=bazelisk
pkgver=1.12.0
pkgrel=1
pkgdesc='A user-friendly launcher for Bazel.'
arch=('x86_64' 'aarch64')
license=('Apache')
url='https://github.com/bazelbuild/bazelisk'
makedepends=('git' 'bash' 'gcc' 'jre11-openjdk' 'python' 'zip' 'unzip')
conflicts=('bazel')
provides=('bazel')
source=("bazelisk-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz")
source_x86_64=(
  "bazelisk-bin-${pkgver}::https://github.com/bazelbuild/bazelisk/releases/download/v${pkgver}/bazelisk-linux-amd64"
)
source_aarch64=(
  "bazelisk-bin-${pkgver}::https://github.com/bazelbuild/bazelisk/releases/download/v${pkgver}/bazelisk-linux-arm64"
)
sha256sums=('5dc7b57c629ec4823cf4b0126b2add8f875ef8015d412ba803e9f566d8480920')
sha256sums_x86_64=('6b0bcb2ea15bca16fffabe6fda75803440375354c085480fe361d2cbf32501db')
sha256sums_aarch64=('29d861ca48df24a3e8dec57fb00508ba664a31921047b2686c38cf9a20d4639f')

prepare() {
  [[ $CARCH == "x86_64" ]] && source_arch=$source_x86_64
  [[ $CARCH == "aarch64" ]] && source_arch=$source_aarch64
  chmod +x "${srcdir}/${source_arch[0]%%::*}"
}

build() {
  cd "bazelisk-${pkgver}"
  [[ $CARCH == "x86_64" ]] && source_arch=$source_x86_64
  [[ $CARCH == "aarch64" ]] && source_arch=$source_aarch64
  "${srcdir}/${source_arch[0]%%::*}" build //:bazelisk
  "${srcdir}/${source_arch[0]%%::*}" shutdown
}

package() {
  cd "bazelisk-${pkgver}"

  install -Dm644 \
    "LICENSE" \
    "${pkgdir}/usr/share/licenses/bazelisk/LICENSE"

  install -Dm755 \
    "bazel-bin/bazelisk_/bazelisk" \
    "${pkgdir}/usr/bin/${pkgname%isk}"
}

